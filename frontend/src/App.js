import React, { Component } from 'react'
import axios from 'axios';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      data: [], //state data diberi tanda [] untuk mendeklarasikan kalau datanya berbentuk array, kalau tidak, maka akan error saap di .map
      dataByNis: {}, //state data diberi tanda [] untuk mendeklarasikan kalau datanya berbentuk objek
      nis: '',
      nama: '',
      kelas: '',
      jk: '',
      alamat: '',
     };
  }

  //fungsi yang dijalankan setelah component berhasil di load di browser
  componentDidMount = () => {
    this.getData();
  }

  // ambil data
  getData = () => {
    //react merequest ke server dengan method dan url dibawah, jika berhasil jalankan fungsi di then, kalau gagal lempar ke catch
    axios.get("http://localhost:8000/siswa").then(res => {
      // hasil request dari database bisa dilihat di browser -> console
      console.log(res)
      this.setState({ data: res.data.results })
    }).catch(err => {
      console.log(err)
    })
  }
  // ambil satu data
  getByNis = a => {
    axios.get("http://localhost:8000/siswa/"+a).then(res => {
      //results berbentuk array, jadi dipanggil indexnya
      this.setState({ dataByNis: res.data.results[0] })
    }).catch(err => {
      console.log(err)
    })
  }
  // kirim data
  postData = () => {
    // buat variabel data untuk menyimpan kumpulan data objek
    const data = {
      nis: this.state.nis,
      nama: this.state.nama,
      kelas: this.state.kelas,
      jk: this.state.jk,
      alamat: this.state.alamat,
    }
    //react melakukan request dengan url dan method dibawah disertai data dari yang di atas.
    axios.post("http://localhost:8000/siswa", data).then(res => {
      console.log(res.data.message);
      this.getData();
    }).catch(err => {
      console.log(err)
    })
  }
  // update data
  putData = () => {
    const data = {
      nis: this.state.nis,
      nama: this.state.nama,
      kelas: this.state.kelas,
      jk: this.state.jk,
      alamat: this.state.alamat,
    }
    axios.put("http://localhost:8000/siswa", data).then(res => {
      console.log("bisa edit");
      this.getData();
    }).catch(err => {
      console.log(err)
    })
  }
  // delete data
  deleteData = nis => {
    const data = {nis: nis}
    axios.post("http://localhost:8000/siswa/delete", data).then(res => {
      console.log(res.data.message);
      console.log(nis)
      this.getData();
    }).catch(err => {
      console.log(err)
    })
  }

  render() {
    return (
      <>
        <table>
          <tr>
            <td>NIS</td>
            <td>Nama</td>
            <td>Kelas</td>
            <td>JK</td>
            <td>Alamat</td>
            <td>Aksi</td>
          </tr>
          {/* melakukan looping untuk menampilkan data satu persatu setiap baris tabel */}
          {this.state.data.map((value, index) => (
            <tr key={index}>
              <td>{value.nis}</td>
              <td>{value.nama}</td>
              <td>{value.kelas}</td>
              <td>{value.jk}</td>
              <td>{value.alamat}</td>
              <td>
                {/* saat akan menjalankan event handler, contohnya onClick atau onChange, wajib menggunakan fungsi walaupun hanya memanggil fungsi lain, seperti arrow function dibawah ini */}
                <button onClick={() => this.getByNis(value.nis)}>Edit</button> 
                <button onClick={() => this.deleteData(value.nis)}>Hapus</button>
              </td>
            </tr>
          ))}
        </table>
        <br />
        <h1>Tambah Data</h1>
        <label>nis</label>
        {/* form input yang ada dibawah ini memiliki properti onchange, yang artinya akan mengupdate state setiap kali valuenya berubah */}
        <input type="text" onChange={ e => this.setState({ nis: e.target.value}) } defaultValue={this.state.nis} /><br />
        <label>nama</label>
        <input type="text" onChange={ e => this.setState({ nama: e.target.value}) } defaultValue={this.state.nama} /><br />
        <label>kelas</label>
        <input type="text" onChange={ e => this.setState({ kelas: e.target.value}) } defaultValue={this.state.kelas} /><br />
        <label>jk</label>
        <input type="text" onChange={ e => this.setState({ jk: e.target.value}) } defaultValue={this.state.jk} /><br />
        <label>alamat</label>
        <textarea type="text" onChange={ e => this.setState({ alamat: e.target.value}) }>{this.state.alamat}</textarea><br />
        <button onClick={() => this.postData()} >Simpan</button>
        <br />
        {/* form edit baru muncul jika tombol edit diklik */}
        {this.state.dataByNis.nis ?
          <>
          <h1>Edit Data</h1>
          <label>nis</label>
          <input type="text" onChange={ e => this.setState({ nis: e.target.value}) } defaultValue={this.state.dataByNis.nis} disabled/><br />
          <label>nama</label>
          <input type="text" onChange={ e => this.setState({ nama: e.target.value}) } defaultValue={this.state.dataByNis.nama} /><br />
          <label>kelas</label>
          <input type="text" onChange={ e => this.setState({ kelas: e.target.value}) } defaultValue={this.state.dataByNis.kelas} /><br />
          <label>jk</label>
          <input type="text" onChange={ e => this.setState({ jk: e.target.value}) } defaultValue={this.state.dataByNis.jk} /><br />
          <label>alamat</label>
          <textarea type="text" onChange={ e => this.setState({ alamat: e.target.value}) }>{this.state.dataByNis.alamat}</textarea><br />
          <button onClick={() => this.putData()} >Edit</button>
          </>
        : null }
      </>
    );
  }
}

export default App;
