const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const cors = require('cors');
//untuk login cara ke 1 dan 2
const session = require('express-session');
//untuk login cara ke 2
const jwt = require('jsonwebtoken')
const app = express();

//konfigurasi koneksi
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'latihan-pas'
});

//connect ke database
conn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Connected...');
});

//untuk terima request
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//set folder untuk menyimpan assets seperti foto
app.use('/assets', express.static(__dirname + '/assets'));
//definisikan session untuk login cara ke 1 dan 2
app.use(session({
    key: 'user_id',
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    cookie: {
        expires: 600000
    }
}));

//==============dua cara untuk login===================
process.env.SECRET_KEY = 'aisahaR' //kode rahasia untuk token

//login biasa, disarankan untuk login aplikasi react native
app.post('/login', (req, res) => {
    let data = { user_id: req.body.user_id, password: req.body.password }
    let sql = `SELECT * FROM user where user_id = ${req.body.user_id} AND password = ${req.body.password}`;
    let query = conn.query(sql, (err, results) => {
        if (!results) {
            res.json({
                message: "Username dan/atau Password Salah",
                sql: sql
            })
        } else {
            req.session.loggedin = true;
            req.session.userId = req.body.user_id;
            res.json({
                message: 'login berhasil',
                user_id: req.body.user_id,
                //variabel results (hasil query) berbentuk array, maka diberi index disampingnya
                results: results[0]
            })
            res.end();
        }
    });
});

//login token, wajib pakai token apabila digunakan di website react. Boleh digunakan di aplikasi react native, tapi yang cara pertama lebih sederhana
app.post('/logintoken', (req, res) => {
    let data = { user_id: req.body.user_id, password: req.body.password }
    let sql = `SELECT * FROM user where user_id = ${req.body.user_id} AND password = ${req.body.password}`;
    let query = conn.query(sql, (err, results) => {
        if (!results) {
            res.json({
                message: "Username dan/atau Password Salah"
            })
        } else {
            //result hasil query akan dienkripsi lalu dikirim di lewat res.send(token)
            //variabel results berbentuk array, maka diberi index disampingnya
            let token = jwt.sign(JSON.parse(JSON.stringify(results[0])), process.env.SECRET_KEY, {
                expiresIn: 1440
            })
            res.send(token)
        }
    });
});
//===============================================================

//route untuk homepage
app.get('/siswa', (req, res) => {
    let sql = "SELECT * FROM siswa";
    let query = conn.query(sql, (err, results) => {
        try {
            res.json({
                results: results
            });
        } catch (err) {
            res.json({
                error: err
            })
        }
    });
});

//route untuk homepage
app.get('/siswa/:nis', (req, res) => {
    let sql = "SELECT * FROM siswa WHERE nis = " + req.params.nis;
    let query = conn.query(sql, (err, results) => {
        try {
            res.json({
                results: results
            });
        } catch (err) {
            res.json({
                error: err
            })
        }
    });
});

//route untuk insert data
app.post('/siswa', (req, res) => {
    let data = { nis: req.body.nis, nama: req.body.nama, kelas: req.body.kelas, jk: req.body.jk, alamat: req.body.alamat };
    let sql = "INSERT INTO siswa SET ?";
    let query = conn.query(sql, data, (err, results) => {
        try {
            res.json({
                message: "Simpan Data Berhasil",
                data: req.body,
            })
        } catch (err) {
            res.json({
                error: err
            })
        }
    });
});

//route untuk update data
app.put('/siswa', (req, res) => {
    let sql = "UPDATE siswa SET nama='" + req.body.nama + "', kelas='" + req.body.kelas + "', jk='" + req.body.jk + "', alamat='" + req.body.alamat + "' WHERE nis=" + req.body.nis;
    let query = conn.query(sql, (err, results) => {
        try {
            res.json({
                message: "Update Data Berhasil",
                data: req.body
            })
        } catch (err) {
            res.json({
                error: err
            })
        }
    });
});

//route untuk delete data
app.post('/siswa/delete', (req, res) => { //tampil, simpan, update, delete memiliki route yang sama namun dengan method yang berbeda, jadi tak akan terjadi konflik
    let sql = "DELETE FROM siswa WHERE nis=" + req.body.nis + "";
    let query = conn.query(sql, (err, results) => {
        try {
            res.json({
                message: "Hapus Data Berhasil",
                id: req.body.nis,
            })
            console.log(sql)
        } catch (err) {
            res.json({
                error: err
            })
        }
    });
});

//server listening
app.listen(8000, () => {
    console.log('Server is running at port 8000');
});